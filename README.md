<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# Oniro Project

Welcome to the Oniro Project bootstrap git repository! You are welcome to take
a tour and play with Oniro's initial code contribution in its final steps
toward becoming the project's official code base. And, if you feel like
joining, we would love to welcome you among the list of Oniro's initiating
supporters. These are exciting times! There couldn't be a better moment for
joining Oniro!

Learn more about the [Oniro Project](https://oniroproject.org/).

Read the [documentation](https://docs.oniroproject.org/).

*\*Oniro is a trademark of Eclipse Foundation.*

# About

The **oniro** repository is a collection of bitbake layers that implement the
build system support in Oniro Project. Check the `README.md` file in each of
the included layers for layer-specific additional information.

The build system documentation is available in the `docs` subdirectory.

## Contributing

See the `CONTRIBUTING.md` file.

## License

See the `LICENSES` subdirectory.
